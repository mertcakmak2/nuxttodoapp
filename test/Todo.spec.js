import { mount } from '@vue/test-utils'
import Todo from '@/components/Todo.vue'

describe('Todo', () => {

    test('title should be "todos" ', () => {
        const wrapper = mount(Todo, {
            propsData: {
                title: 'todos'
            }
        })

        // Expected: "test"
        // Received: "todos"
        //expect(wrapper.vm.title).toBe('test')   //  => Failed test

        expect(wrapper.vm.title).toBe('todos')    //  => Success test
    })

    test('todo list test ', async () => {
        const wrapper = mount(Todo)

        wrapper.vm.$emit('todos', { asd: 11, description: "nuxtdan", statu: "1" })
        wrapper.vm.$emit('todos', { asd: 1, description: "go lang çalışması", statu: "0" })

        await wrapper.vm.$nextTick()

        // Expected: 3
        // Received: 2
        //expect(wrapper.emitted().todos.length).toBe(3) // => Failed test

        expect(wrapper.emitted().todos.length).toBe(2) // => Success test
    })

    test('todo statu test ', async () => {
        const wrapper = mount(Todo)

        // wrapper.vm.$emit('todos', { asd: 1, description: "go lang çalışması", statu: "3" }) // => Failed test
        wrapper.vm.$emit('todos', { asd: 1, description: "go lang çalışması", statu: "1" })    // => Success test

        await wrapper.vm.$nextTick()

        const todos = wrapper.emitted().todos[0]

        const expected = [todos[0].statu];

        expect(['0', '1']).toEqual(
            expect.arrayContaining(expected)
        );

    })
})
